package org.tgh.chat01.model;

import lombok.Data;

/**
 * @USER: 松树戈
 * @DATE: 2021/12/13 19:51
 */
@Data
public class Chat {
    private String to;
    private String from;
    private String content;
}

package org.tgh.chat01.model;

import lombok.Data;

/**
 * @USER: 松树戈
 * @DATE: 2021/12/13 14:03
 */
@Data
public class Message {
    private String name;
    private String content;
}

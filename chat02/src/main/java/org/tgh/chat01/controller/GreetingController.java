package org.tgh.chat01.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.tgh.chat01.model.Chat;
import org.tgh.chat01.model.Message;

import java.security.Principal;

/**
 * @USER: 松树戈
 * @DATE: 2021/12/13 14:02
 */
@Controller
public class GreetingController {
    //发消息用
    @Autowired
    SimpMessagingTemplate simpMessagingTemplate;
    //客户端消息往@MessageMapping地址上发
    //前端页面根据@SendTo定义监听地址 （广播地址）
    @MessageMapping("/hello")
    @SendTo("/topic/greetings")
    public Message greeting(Message message) {
        System.out.println("message ="+message);
        return message;
    }

    /**
     *
     * @param principal 用户信息
     * @param chat 消息信息
     */
    @MessageMapping("/online_chat")
    public void chat(Principal principal, Chat chat) {
        //当前登录用户是谁就是谁发的。
        String from = principal.getName();
        chat.setFrom(from);

        simpMessagingTemplate.convertAndSendToUser(chat.getTo(),"/queue/chat",chat);
    }
}

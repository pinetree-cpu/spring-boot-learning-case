package org.tgh.mail;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.tgh.mail.model.User;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Date;

@SpringBootTest
class MailApplicationTests {
    @Autowired
    JavaMailSender javaMailSender;

    @Test
    void contextLoads() {
        SimpleMailMessage simMsg = new SimpleMailMessage();
        //描述邮件基本信息
        simMsg.setFrom("3425928061@qq.com");
        simMsg.setTo("3425928061@qq.com");
        simMsg.setSubject("邮件主题--测试邮件");
        simMsg.setText("邮件内容-测试邮件");
        javaMailSender.send(simMsg);
    }

    @Test
    void test1() throws MessagingException {
        File file = new File("E:/java集合框架图.gif");
        //复合邮件
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        //heper 是一个辅助工具
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
        helper.setFrom("3425928061@qq.com");
        helper.setTo("3425928061@qq.com");
        helper.setSubject("邮件主题--测试邮件");
        helper.setText("邮件内容-测试邮件");
        //文件名字 文件对象
        helper.addAttachment(file.getName(),file);
        javaMailSender.send(mimeMessage);
    }

    @Test
    void test2() throws MessagingException {
        File file = new File("E:/java集合框架图.gif");
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        //heper 是一个辅助工具
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
        helper.setFrom("3425928061@qq.com");
        helper.setTo("3425928061@qq.com");
        helper.setSentDate(new Date());
        helper.setSubject("邮件主题--测试邮件");
        helper.setText("<div>hello , 这是一封带图片资源的邮件...</div><div><img src='cid:p01'></div>",true);
        // p01 : 标记  file : 图片对象
        helper.addInline("p01",file);
        javaMailSender.send(mimeMessage);
    }

    @Test
    void test3() throws MessagingException, IOException, TemplateException {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        //heper 是一个辅助工具
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
        helper.setFrom("3425928061@qq.com");
        helper.setTo("3425928061@qq.com");
        helper.setSentDate(new Date());
        helper.setSubject("邮件主题--测试邮件");
        Configuration cfg = new Configuration(Configuration.VERSION_2_3_30);
        cfg.setClassLoaderForTemplateLoading(MailApplicationTests.class.getClassLoader(), "mail");
        Template template = cfg.getTemplate("mail.ftl");
        User user = new User();
        //输出流 渲染之后的页面内容
        user.setCompany("xxx公司");
        user.setUsername("小明");
        user.setPosition("品保");
        user.setSalary(8899.0);
        StringWriter out = new StringWriter();
        template.process(user,out);
        String text = out.toString();
        System.out.println("text =  " + text);
        helper.setText(text,true);
        javaMailSender.send(mimeMessage);
    }

    @Autowired
    TemplateEngine templateEngine;
    @Test
    void test4() throws MessagingException, IOException, TemplateException {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        //heper 是一个辅助工具
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
        helper.setFrom("3425928061@qq.com");
        helper.setTo("3425928061@qq.com");
        helper.setSentDate(new Date());
        helper.setSubject("邮件主题--测试邮件");
        Context context = new Context();
        context.setVariable("username","小明");
        context.setVariable("position","工程");
        context.setVariable("company","xx集团");
        context.setVariable("salary","99889");
        String text = templateEngine.process("mail.html", context);
        helper.setText(text,true);
        javaMailSender.send(mimeMessage);
    }
}

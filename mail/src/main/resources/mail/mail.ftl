<div>欢迎 ${username} 加入 ${company} 大家庭，您的入职信息如下：</div>
<table>
    <tr>
        <td>姓名</td>
        <td>${username}</td>
    </tr>
    <tr>
        <td>职位</td>
        <td>${position}</td>
    </tr>
    <tr>
        <td>薪水</td>
        <td>${salary}</td>
    </tr>
</table>
<div style="color: #ff3f57;font-size: large">希望在未来的日子里携手奋进！</div>
<style>
    table {
        border-collapse: collapse;
    }
    tr, td {
        border: 1px solid black;
        padding: 5px;
        text-align: center;
    }
    tr:nth-child(even) {background-color: #f2f2f2;}
</style>
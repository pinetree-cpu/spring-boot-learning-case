package org.tgh.mail.model;

import lombok.Data;

/**
 * @USER: 松树戈
 * @DATE: 2021/12/18 20:09
 */
@Data
public class User {
    private String username;
    private Double salary;
    private String position;
    private String company;
}

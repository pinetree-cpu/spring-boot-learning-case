package org.tgh.scheduled;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @USER: 松树戈
 * @DATE: 2021/12/19 9:43
 */
@Component
public class MySchedule {
    //当前方法执行结束一秒过后，开启下一个任务
    @Scheduled(fixedDelay = 1000)
    public void fixedDelay() {
        System.out.println("fixedDelay:" + new Date());
    }

    //当前方法开始执行一秒过后，开启下一个任务
    @Scheduled(fixedRate = 1000)
    public void fixedRate() {
        System.out.println("fixedRate:" + new Date());
    }

    @Scheduled(initialDelay = 1000,fixedDelay = 1000)
    public void initDelay() {
        System.out.println("initDelay:" + new Date());
    }

    @Scheduled(cron = "0/5 * * * * *")
    public void cron() {
        System.out.println("cron:" + new Date());
    }
}

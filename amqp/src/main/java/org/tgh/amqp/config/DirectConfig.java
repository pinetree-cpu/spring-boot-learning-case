package org.tgh.amqp.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @USER: 松树戈
 * @DATE: 2021/12/17 17:07
 */
@Configuration
public class DirectConfig {
    //队列
    @Bean
    Queue directQueue() {
        return new Queue("t2-queue1");
    }

    //新增队列
    @Bean
    Queue directQueue2() {
        return new Queue("t2-queue2");
    }
    // 如果用来direct模式，下面两个bean可以省略。
    //交换机
    @Bean
    DirectExchange directExchange() {
        //参数二：重启之后队列是否依然有效 参数三：长期未使用的时候是否自动删除
        return new DirectExchange("t2-direct",true,false);
    }


    //作用：将队列和交换机绑定到一起
    @Bean
    Binding directBinding() {
        return BindingBuilder.bind(directQueue()).to(directExchange()).with("direct1");
    }

    @Bean
    Binding directBinding2() {
        return BindingBuilder.bind(directQueue2()).to(directExchange()).with("direct2");
    }
}

package org.tgh.amqp.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.HeadersExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * @USER: 松树戈
 * @DATE: 2021/12/17 20:24
 */
@Configuration
public class HeaderConfig {
    @Bean
    Queue queueAge() {
        return new Queue("queue-age");
    }

    @Bean
    Queue queueName() {
        return new Queue("queue-name");
    }

    @Bean
    HeadersExchange headersExchange() {
        return new HeadersExchange("t2-header",true,false);
    }

    @Bean
    Binding bindingAge() {
        Map<String, Object> map = new HashMap<>();
        map.put("age",99);
        return BindingBuilder.bind(queueAge()).to(headersExchange()).whereAny(map).match();
    }

    @Bean
    Binding bindingName() {
        return BindingBuilder.bind(queueName()).to(headersExchange()).where("name").exists();
    }
}

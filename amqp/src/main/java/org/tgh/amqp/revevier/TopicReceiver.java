package org.tgh.amqp.revevier;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @USER: 松树戈
 * @DATE: 2021/12/17 20:13
 */
@Component
public class TopicReceiver {
    @RabbitListener(queues = "phone")
    public void handler1(String msg) {
        System.out.println("phone-msg = " + msg);
    }

    @RabbitListener(queues = "xiaomi")
    public void handler2(String msg) {
        System.out.println("xiaomi-msg = " + msg);
    }

    @RabbitListener(queues = "huawei")
    public void handler3(String msg) {
        System.out.println("huawei-msg = " + msg);
    }
}

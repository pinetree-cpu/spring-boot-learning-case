package org.tgh.amqp.revevier;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @USER: 松树戈
 * @DATE: 2021/12/17 20:32
 */
@Component
public class HeaderReceiver {
    @RabbitListener(queues = "queue-age")
    public void handler1(String msg) {
        System.out.println("queue-age:msg = " + msg);
    }

    @RabbitListener(queues = "queue-name")
    public void handler2(String msg) {
        System.out.println("queue-name:msg = " + msg);
    }
}

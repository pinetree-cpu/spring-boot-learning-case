package org.tgh.amqp.revevier;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @USER: 松树戈
 * @DATE: 2021/12/17 17:14
 */
@Component
public class DirectReceiver {
    @RabbitListener(queues = "t2-queue1")
    public void handler(String msg) {
        System.out.println("msg1 = " + msg);
    }

    @RabbitListener(queues = "t2-queue2")
    public void handler2(String msg) {
        System.out.println("msg2 = " + msg);
    }
}

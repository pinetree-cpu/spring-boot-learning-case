package org.tgh.amqp;

import org.junit.jupiter.api.Test;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.nio.charset.StandardCharsets;

@SpringBootTest
class AmqpApplicationTests {

    @Autowired
    RabbitTemplate rabbitTemplate;
    @Test
    void contextLoads() {
//        rabbitTemplate.convertAndSend("direct","t2-queue", "hello direct1");

//        rabbitTemplate.convertAndSend("t2-queue1","hello direct1");
//        rabbitTemplate.convertAndSend("t2-queue2","hello direct2");
//        rabbitTemplate.convertAndSend("t2-fanout",null,"hello fanout");
//        rabbitTemplate.convertAndSend("t2-topic","xiaomi.new","小米新闻");
//        rabbitTemplate.convertAndSend("t2-topic","huawei.watch","华为手表");
//        rabbitTemplate.convertAndSend("t2-topic","apple.phone","苹果手机");
//        rabbitTemplate.convertAndSend("t2-topic","phone.apple","手机苹果");

        Message nameMsg = MessageBuilder.withBody("hello you".getBytes()).setHeader("name","t2-header").build();
        Message ageMsg = MessageBuilder.withBody("hello you".getBytes()).setHeader("age",99).build();
        rabbitTemplate.send("t2-header",null,nameMsg);
        rabbitTemplate.send("t2-header",null,ageMsg);
    }

}

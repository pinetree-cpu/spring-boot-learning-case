package org.tgh.jms.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.stereotype.Component;
import org.tgh.jms.model.Message;

import javax.jms.Queue;

/**
 * @USER: 松树戈
 * @DATE: 2021/12/17 15:00
 */
@Component
public class JmsComponent {
    //消息发送模板
    @Autowired
    JmsMessagingTemplate jmsMessagingTemplate;

    @Autowired
    Queue queue;

    public void send(Message message) {
        //参数一：发送目的地 参数二：消息内容 参数三：
        jmsMessagingTemplate.convertAndSend(queue, message);
    }

    //指明接收哪儿的消息
    @JmsListener(destination = "t2-queue")
    public void receive(Message msg) {
        System.out.println("msg = " + msg);
    }
}

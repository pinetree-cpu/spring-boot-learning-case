package org.tgh.jms.model;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @USER: 松树戈
 * @DATE: 2021/12/17 15:02
 */
//传输需要序列化
@Data
public class Message implements Serializable {
    private String content;
    private Date date;
}

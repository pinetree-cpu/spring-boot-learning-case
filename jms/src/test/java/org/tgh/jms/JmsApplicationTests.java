package org.tgh.jms;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.tgh.jms.config.JmsComponent;
import org.tgh.jms.model.Message;

import java.util.Date;

@SpringBootTest
class JmsApplicationTests {

    @Autowired
    JmsComponent jmsComponent;
    @Test
    void contextLoads() {
        Message message = new Message();
        message.setContent("hello world");
        message.setDate(new Date());
        jmsComponent.send(message);

    }

}

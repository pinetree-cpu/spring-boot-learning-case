package org.tgh.quarzt.job;

import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @USER: 松树戈
 * @DATE: 2021/12/19 10:46
 */
@Component
public class MyJob01 {
    public void sayHello() {
        System.out.println("MyJob01:" + new Date());
    }
}

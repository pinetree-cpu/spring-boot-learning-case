package org.tgh.chat01.controller;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.tgh.chat01.model.Message;

/**
 * @USER: 松树戈
 * @DATE: 2021/12/13 14:02
 */
@Controller
public class GreetingController {

    //前端页面根据@SendTo定义监听地址 （广播地址）
    @MessageMapping("/hello")
    @SendTo("/topic/greetings")
    public Message greeting(Message message) {
        System.out.println("message ="+message);
        return message;
    }
}
